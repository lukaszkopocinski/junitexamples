package pl.droidsonroids.bootcamp.bootcampjunit

class Player(val name: String = "", scoredGoals: Int = 0) {

    var isInjured: Boolean = false
        private set

    var scoredGoals: Int = scoredGoals
        private set

    fun setInjured(daysOff: Int) {
        isInjured = true
    }

    fun scoreGoals(count: Int) {
        scoredGoals += count
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Player

        if (name != other.name) return false
        if (isInjured != other.isInjured) return false
        if (scoredGoals != other.scoredGoals) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + isInjured.hashCode()
        result = 31 * result + scoredGoals
        return result
    }
}
