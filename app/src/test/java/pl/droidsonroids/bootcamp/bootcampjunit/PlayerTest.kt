package pl.droidsonroids.bootcamp.bootcampjunit

import org.junit.AfterClass
import org.junit.Assert.*
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

class PlayerTest {

    companion object {
        @BeforeClass @JvmStatic
        fun methodBeforeClass() {
            // this method is executed only once before all tests
            println("Only Once Before")
        }

        @AfterClass @JvmStatic
        fun methodAfterClass() {
            // this method is executed only once after all tests
            println("Only Once After")
        }
    }

    private lateinit var player: Player
    //val player = Player()

    @Before
    fun setUp() {
        player = Player()
        println("Before each test")
    }

    @Test
    fun testSetPlayerInjured() {
        player.setInjured(daysOff = 8)

        assertTrue(player.isInjured)
    }

    @Test
    fun testPlayerInjured() {
        assertFalse(player.isInjured)
    }

    @Test
    fun testScoreGoals() {
        val player = Player("Neymar", 5)

        player.scoreGoals(7)

        assertEquals(12, player.scoredGoals)
    }

    @Test
    fun testIsSamePlayer() {
        val player = Player("Ronaldo")
        //val player2 = player

        assertSame(player, player)
    }

    @Test
    fun testIsNotSamePlayer() {
        val player = Player("Ronaldo")
        val player2 = Player("Ronaldo")

        assertNotSame(player, player2)
        assertSame(player, player2)
    }

    @Test
    fun testArePlayersEquals() {
        val player = Player("Ronaldo")
        val player2 = Player("Ronaldo")

        assertEquals(player, player2)
    }
}